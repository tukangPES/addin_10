﻿Option Strict On

Public Class frmTypeSelector

    Private _appObj As EnvDTE80.DTE2
    Private _assem As System.Reflection.Assembly
    Private _instance As Object
    Private _margin As Integer
    Private _generatedClass As System.ComponentModel.IComponent
    Private _tabControl As System.Windows.Forms.TabControl
    Private _top As Integer = 0
    Private _objectCounter As Integer
    Private _daftarPropFiller As New System.Collections.Generic.List(Of String)

    <Flags()> _
    Enum AutoCompleteFlags As Integer
        SHACF_DEFAULT = &H0     ' Currently (SHACF_FILESYSTEM | SHACF_URLALL)
        SHACF_FILESYSTEM = &H1      ' This includes the File System as well as the rest of the shell (Desktop\My Computer\Control Panel\)
        SHACF_URLALL = (SHACF_URLHISTORY Or SHACF_URLMRU)
        SHACF_URLHISTORY = &H2      ' URLs in the User's History
        SHACF_URLMRU = &H4      ' URLs in the User's Recently Used list.
        SHACF_USETAB = &H8      ' Use the tab to move thru the autocomplete possibilities instead of to the next dialog/window control.
        SHACF_FILESYS_ONLY = &H10   ' This includes the File System
        SHACF_FILESYS_DIRS = &H20   ' Same as SHACF_FILESYS_ONLY except it only includes directories, UNC servers, and UNC server shares.
        SHACF_AUTOSUGGEST_FORCE_ON = &H10000000       ' Ignore the registry default and force the feature on.
        SHACF_AUTOSUGGEST_FORCE_OFF = &H20000000      ' Ignore the registry default and force the feature off.
        SHACF_AUTOAPPEND_FORCE_ON = &H40000000    ' Ignore the registry default and force the feature on. (Also know as AutoComplete)
        SHACF_AUTOAPPEND_FORCE_OFF = &H80000000       ' Ignore the registry default and force the feature off. (Also know as AutoComplete)
    End Enum
    Private Declare Function SHAutoComplete Lib "shlwapi.dll" (ByVal hWndEdit As IntPtr, ByVal dwFlags As AutoCompleteFlags) As Integer

    Private Sub frmTypeSelector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.DesignMode Then
            Return
        End If
        SHAutoComplete(TextBox1.Handle, 0)
    End Sub

    Public Sub New(ByVal iApplicationObject As EnvDTE80.DTE2)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _appObj = iApplicationObject
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim aCounter = 0
        TextBox1.Text = Strings.Replace(TextBox1.Text, """", "")
        Dim aPFNcopy = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), IO.Path.GetFileName(TextBox1.Text))

        Do While IO.File.Exists(aPFNcopy)
            aCounter += 1
            aPFNcopy = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), aCounter & "_" & IO.Path.GetFileName(TextBox1.Text))
        Loop
        Try
            My.Computer.FileSystem.CopyFile(TextBox1.Text, aPFNcopy, FileIO.UIOption.OnlyErrorDialogs)

            _assem = System.Reflection.Assembly.LoadFrom(aPFNcopy)

            For Each aMod In _assem.GetTypes()
                ComboBox1.Items.Add(aMod)
            Next

        Catch exLoader As System.Reflection.ReflectionTypeLoadException

        Catch ex As Exception



        End Try

        ComboBox1.DroppedDown = True

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        _objectCounter = 0

        Dim aHost = CType(_appObj.ActiveWindow.Object, System.ComponentModel.Design.IDesignerHost)
        Dim aErr = ""

        If aErr <> "" Then
            MsgBox(aErr)
        End If

        If aErr <> "" AndAlso MsgBox("mau dilanjutkan ?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        Else
            processThisnode(TreeView1.Nodes(0))
        End If



    End Sub

    Private Sub processThisnode(ByVal iNode As TreeNode)
        Dim aPropertyInfo As Reflection.PropertyInfo
        Dim aComponent As System.Windows.Forms.Control

        For Each aNode As TreeNode In iNode.Nodes
            If aNode.Checked Then
                aPropertyInfo = CType(aNode.Tag, Reflection.PropertyInfo)
                
                '==> create labelnya
                Me.addWindowsControl(GetType(customControl.myLabel), aNode.Text, Nothing)

                '==> create controlnya
                Dim aCreateDGV = False
                Dim aIsListOf = False
                If aPropertyInfo.PropertyType.IsEnum Then
                    aComponent = Me.addWindowsControl(GetType(ComboBox), aNode.Text, Nothing)
                    With CType(aComponent, ComboBox)
                        .DropDownStyle = ComboBoxStyle.DropDownList
                        For Each aName In [Enum].GetNames(aPropertyInfo.PropertyType)
                            .Items.Add(aName)
                        Next
                    End With

                ElseIf aPropertyInfo.PropertyType.Equals(GetType(Double)) Then
                    aComponent = Me.addWindowsControl(GetType(lib_customControl_02.clsMyTextBox), aNode.Text, Nothing)

                ElseIf aPropertyInfo.PropertyType.Equals(GetType(Integer)) Then
                    aComponent = Me.addWindowsControl(GetType(lib_customControl_02.clsMyTextBox), aNode.Text, Nothing)

                ElseIf aPropertyInfo.PropertyType.Equals(GetType(String)) Then
                    aComponent = Me.addWindowsControl(GetType(lib_customControl_02.clsMyTextBox), aNode.Text, Nothing)

                ElseIf aPropertyInfo.PropertyType.Equals(GetType(Boolean)) Then
                    aComponent = Me.addWindowsControl(GetType(CheckBox), aNode.Text, Nothing)

                ElseIf aPropertyInfo.PropertyType.Equals(GetType(Date)) Then
                    aComponent = Me.addWindowsControl(GetType(lib_customControl_02.clsMyDate), aNode.Text, Nothing)

                ElseIf aPropertyInfo.PropertyType.IsGenericType() Then
                    'kalau property itu jenisnya list of
                    aCreateDGV = True
                    aIsListOf = True

                ElseIf aPropertyInfo.PropertyType.HasElementType Then
                    'kalau property itu jenisnya array 
                    aCreateDGV = True
                    aIsListOf = False
                End If

                If aCreateDGV Then
                    If _tabControl Is Nothing Then
                        _tabControl = CType(Me.addWindowsControl(GetType(TabControl), "theTabControl", Nothing), TabControl)
                    End If

                    '==> generate dgv nya
                    Dim aTabPage = New TabPage
                    _tabControl.TabPages.Add(aTabPage)
                    aTabPage.Text = aPropertyInfo.Name
                    Dim aDGV = Me.addWindowsControl(GetType(lib_customControl_02.clsDGV), aNode.Text, aTabPage)

                    Dim aTypeDariPropertyMembernya As System.Type
                    If aIsListOf Then
                        aTypeDariPropertyMembernya = aPropertyInfo.PropertyType.GetGenericArguments()(0)
                    Else
                        aTypeDariPropertyMembernya = aPropertyInfo.PropertyType.GetElementType()
                    End If

                    With CType(aDGV, lib_customControl_02.clsDGV)
                        Dim aNamaKolom = ""
                        Dim aNamaProperty = ""
                        .namaProperty = aNode.Text

                        For Each aProp As Reflection.PropertyInfo In aTypeDariPropertyMembernya.GetProperties()
                            Dim aCol = New lib_customControl_02.clsColumn
                            aCol.Name = "_col" & aProp.Name
                            aCol.Name = Strings.Replace(aCol.Name, " ", "")
                            aCol.Name = Strings.Replace(aCol.Name, ".", "_")
                            aCol.HeaderText = aProp.Name

                            Dim aPropertyParent = System.ComponentModel.TypeDescriptor.GetProperties(aCol)("namaProperty")
                            aPropertyParent.SetValue(aCol, aProp.Name)

                            aCol.namaProperty = aProp.Name
                            .Columns.Add(aCol)
                        Next

                        .Dock = DockStyle.Fill

                    End With
                End If

                '==> naikkan top margin
                _top += 23
            End If
            processThisnode(aNode)
        Next
    End Sub

    Private Function addPropDesc02(ByVal iPropInfo As Reflection.PropertyInfo, ByVal iFullNameSpacePropertyName As String) As System.ComponentModel.IComponent
        'propertyDescriptor
        _objectCounter += 1
        Dim aHost = CType(_appObj.ActiveWindow.Object, System.ComponentModel.Design.IDesignerHost)
        Dim aNamaControl = "pd_" & Strings.Replace(iFullNameSpacePropertyName, ".", "_") & _objectCounter
        Dim aPropDesc As System.ComponentModel.IComponent = aHost.CreateComponent(GetType(lib_livanStandardizer.clsPropertyInfoDescriptor), aNamaControl)
        With CType(aPropDesc, lib_livanStandardizer.clsPropertyInfoDescriptor)
            .namaProperty = iFullNameSpacePropertyName
        End With

        Return aPropDesc
    End Function

    Private Function addPropertyFiller(ByVal iForControl As Control, ByVal iPropDesc As System.ComponentModel.IComponent) As System.ComponentModel.IComponent
        _objectCounter += 1
        Dim aHost = CType(_appObj.ActiveWindow.Object, System.ComponentModel.Design.IDesignerHost)
        Dim aNamaProperty = CType(iPropDesc, lib_livanStandardizer.clsPropertyInfoDescriptor).namaProperty
        Dim aNama = "pf_" & Strings.Replace(aNamaProperty, ".", "_") & _objectCounter
        Dim aPropFiller As System.ComponentModel.IComponent = aHost.CreateComponent(GetType(lib_livanStandardizer.clsPropertyFiller), aNama)

        _daftarPropFiller.Add(aNama)


        If iPropDesc Is Nothing Then
        Else
            'propDescriptor
            Dim aPropDescriptor = System.ComponentModel.TypeDescriptor.GetProperties(aPropFiller)("propDescriptor")
            aPropDescriptor.SetValue(aPropFiller, iPropDesc)
        End If

        'assignedControl
        Dim aAssignedControl = System.ComponentModel.TypeDescriptor.GetProperties(aPropFiller)("assignedControl")
        aAssignedControl.SetValue(aPropFiller, iForControl)

        Return aPropFiller
    End Function

    Private Function addWindowsControl(ByVal iTypeControl As Type, ByVal iNamaControl As String, ByVal iRoot As System.ComponentModel.IComponent) As Control
        _objectCounter += 1
        Dim aHost = CType(_appObj.ActiveWindow.Object, System.ComponentModel.Design.IDesignerHost)
        Dim aNamaControlAsli = iNamaControl
        iNamaControl = Strings.Replace(iNamaControl, ".", "_")
        Dim aNama = ""
        If iTypeControl Is GetType(TextBox) OrElse iTypeControl.IsAssignableFrom(GetType(lib_customControl_02.clsMyTextBox)) Then
            aNama = "txt" & iNamaControl

        ElseIf iTypeControl Is GetType(Label) OrElse iTypeControl.IsAssignableFrom(GetType(customControl.myLabel)) Then
            aNama = "lbl" & iNamaControl

        ElseIf iTypeControl Is GetType(CheckBox) Then
            aNama = "chk" & iNamaControl

        ElseIf iTypeControl Is GetType(DateTimePicker) OrElse iTypeControl.IsAssignableFrom(GetType(lib_customControl_02.clsMyDate)) Then
            aNama = "tgl" & iNamaControl

        ElseIf iTypeControl Is GetType(TabControl) Then
            aNama = "tab" & iNamaControl

        ElseIf iTypeControl Is GetType(DataGridView) Then
            aNama = "dgv" & iNamaControl

        Else
            aNama = iNamaControl
        End If
        aNama &= _objectCounter

        Dim aNewComponent As System.ComponentModel.IComponent = aHost.CreateComponent(iTypeControl, aNama)

        '==> setting parent dan topnya
        If TypeOf aNewComponent Is Control Then
            With CType(aNewComponent, Control)
                .Top = _top
            End With

            Dim aRootComponent As System.ComponentModel.IComponent
            If iRoot Is Nothing Then
                aRootComponent = aHost.RootComponent
            Else
                aRootComponent = iRoot
            End If


            If TypeOf aNewComponent Is Control Then
                With CType(aNewComponent, Control)
                    .Parent = CType(aRootComponent, Control)
                End With
            End If

        End If

        '==> settting default value
        If TypeOf aNewComponent Is enumAndType.iClearAbleControl Then
            CType(aNewComponent, enumAndType.iClearAbleControl).isUseRefresRule = True
        End If
        If TypeOf aNewComponent Is enumAndType.iIsDiplayableControl Then
            With CType(aNewComponent, enumAndType.iIsDiplayableControl)
                .isDisplay = True
                .isUseIsDisplayRule = True
            End With
        End If

        '=> setting leftnya
        If (TypeOf aNewComponent Is TextBox) OrElse (TypeOf aNewComponent Is DateTimePicker) Then
            With CType(aNewComponent, Control)
                .Width = 100
                .Left = 100 + _margin
            End With

        ElseIf TypeOf aNewComponent Is Label Then
            With CType(aNewComponent, Label)
                .AutoSize = False
                .Width = 120
                .Height = 20
                .Left = 0 + _margin
                .Text = getCaptionLabel(iNamaControl)
                .SendToBack()
            End With
        End If

        Dim aPropInfo = iTypeControl.GetProperty("namaProperty",
                                    Reflection.BindingFlags.IgnoreCase Or
                                    Reflection.BindingFlags.Public Or
                                    Reflection.BindingFlags.NonPublic Or
                                    Reflection.BindingFlags.Instance)
        If aPropInfo IsNot Nothing Then
            aPropInfo.SetValue(aNewComponent, aNamaControlAsli, Nothing)
        End If

        aPropInfo = iTypeControl.GetProperty("namaKelas",
                                    Reflection.BindingFlags.IgnoreCase Or
                                    Reflection.BindingFlags.Public Or
                                    Reflection.BindingFlags.NonPublic Or
                                    Reflection.BindingFlags.Instance)
        If aPropInfo IsNot Nothing Then
            aPropInfo.SetValue(aNewComponent, "_kelas", Nothing)
        End If



        Return CType(aNewComponent, Control)

    End Function

    Private Function getCaptionLabel(ByVal iString As String) As String
        Dim aStartLower = Asc("a")
        Dim aEndLower = Asc("z")
        Dim aReturn = ""
        Dim aCurrentIsLower = True
        Dim aFirst = True

        For Each aChar In iString
            Dim aAsc = Asc(aChar)

            If aAsc >= aStartLower AndAlso aAsc <= aEndLower Then
                aReturn &= aChar
            ElseIf (Not aFirst) Then
                aReturn &= " " & aChar
            End If
            If aFirst Then
                aFirst = False
            End If
        Next
        aReturn = Strings.StrConv(aReturn, vbProperCase)
        Return aReturn
    End Function

    Private Sub tblExamine_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tblExamine.Click
        Dim aSelectedType = CType(ComboBox1.SelectedItem, System.Type)
        TreeView1.Nodes.Clear()
        Dim aRoot As TreeNode
        aRoot = New TreeNode With {.Text = "Root Kelas"}
        TreeView1.Nodes.Add(aRoot)
        Try
            alkhalshg(aSelectedType, aRoot, "", 0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub alkhalshg(ByVal iType As System.Type, ByVal iParentNode As TreeNode, ByVal iNameSpace As String, ByVal iCurrentLevel As Integer)
        Dim aNode As TreeNode
        If iCurrentLevel > 10 Then
            Return
        End If
        For Each aProperty In iType.GetProperties()
            aNode = New TreeNode With {.Text = aProperty.Name}
            aNode.Tag = aProperty
            If iNameSpace <> "" Then
                aNode.Text = iNameSpace & "." & aNode.Text
            End If
            iParentNode.Nodes.Add(aNode)

            If aProperty.PropertyType.IsEnum Then
            ElseIf aProperty.PropertyType.Equals(GetType(Double)) Then
            ElseIf aProperty.PropertyType.Equals(GetType(String)) Then
            ElseIf aProperty.PropertyType.Equals(GetType(Boolean)) Then
            ElseIf aProperty.PropertyType.Equals(GetType(Date)) Then
            ElseIf aProperty.PropertyType.IsGenericType() Then
            ElseIf aProperty.PropertyType.HasElementType Then
            Else
                Try
                    Me.alkhalshg(aProperty.PropertyType, aNode, aProperty.Name, iCurrentLevel + 1)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If

        Next
        iParentNode.Expand()
    End Sub
End Class

